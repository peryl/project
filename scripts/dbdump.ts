import * as nconf from "nconf";
import * as fs from "fs";
import * as path from "path";
import { confDefault, Conf } from "../src/conf";
import { objPaths } from "../src/prest/objpaths";
import { DbStore, DbStoreConf } from "../src/store/dbstore";

nconf
    .argv()
    .env({
        separator: "_",
        lowerCase: false,
        whitelist: objPaths(confDefault).map(c => c.join("_")),
        parseValues: true
    })
    .file(path.join(__dirname, "..", "conf.json"))
    .defaults(confDefault);

const conf = nconf.get() as Conf;

console.log("conf: %s", JSON.stringify(conf, null, 4));

const dbStore = new DbStore({ url: path.join(__dirname, "..", conf.db.url) } as DbStoreConf);

(async () => {
    const data = await dbStore.dumpAll();
    const name = `backup-${new Date().toISOString()}`;
    const p = path.join(conf.dbdump, name);
    console.log("DB dump", p, " - ",
        Object.keys(data)
            .map(k => `${k}: ${data[k].length}`)
            .join(", "));
    fs.writeFileSync(p, JSON.stringify(data, null, 4));
})();
