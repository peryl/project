import * as nconf from "nconf";
import * as fs from "fs";
import * as path from "path";
import { confDefault, Conf } from "../src/conf";
import { objPaths } from "../src/prest/objpaths";
import { DbStore, DbStoreConf } from "../src/store/dbstore";

nconf
    .argv()
    .env({
        separator: "_",
        lowerCase: false,
        whitelist: objPaths(confDefault).map(c => c.join("_")),
        parseValues: true
    })
    .file(path.join(__dirname, "..", "conf.json"))
    .defaults(confDefault);

const conf = nconf.get() as Conf;

console.log("conf: %s", JSON.stringify(conf, null, 4));

const dbStore = new DbStore({ url: path.join(__dirname, "..", conf.db.url) } as DbStoreConf);

(async () => {
    const name = process.argv[2];
    console.log("load", name);
    const p = path.join(conf.dbdump, name);
    const json = fs.readFileSync(p, "utf8");
    const data = JSON.parse(json);
    console.log("DB load", p, " - ",
        Object.keys(data)
            .map(k => `${k}: ${data[k].length}`)
            .join(", "));
    await Promise.all(Object.keys(data).map(async collection => {
        console.log("remove all", collection);
        await dbStore.removeAll(collection);
        console.log("removed all", collection);
    }));
    await Promise.all(Object.keys(data).map(async collection => {
        console.log("load all", collection, data[collection].length);
        await dbStore.load(collection, data[collection]);
        console.log("loaded all", collection, data[collection].length);
    }));
    const dump = await dbStore.dumpAll();
    Object.keys(dump).forEach(k => {
        console.log("status", k, dump[k].length);
    });
})();
