import * as log4js from "log4js";
import { basename, extname } from "path";
import * as path from "path";
import * as express from "express";
import { generateRoutes, generateSpec, Config } from "tsoa";
import { ExtendedRoutesConfig, ExtendedSpecConfig } from "tsoa/dist/cli";
// import { authBasicOrJwt } from "./middleware/authBasicOrJwt";
import * as swaggerUi from "swagger-ui-express";
import * as cors from "cors";
import * as bodyParser from "body-parser";

const log = log4js.getLogger(basename(__filename, extname(__filename)));

const pkg = require(path.resolve(__dirname, "../package.json"));

export function api(app: express.Express) {
    (async () => {
        const swaggerOutputDirectory = "public/api";

        const conf = app.conf;

        const commonConfig = {
            entryFile: "src/app.ts",
            noImplicitAdditionalProperties: "silently-remove-extras" as Config["noImplicitAdditionalProperties"],
            controllerPathGlobs: ["src/api/**/*.controller.ts"]
        };

        const specConfig: ExtendedSpecConfig = {
            ...commonConfig,
            specVersion: 3,
            name: "Notification",
            description: "Notification service",
            schemes: conf.api.spec.schemes,
            host: conf.api.spec.host,
            basePath: conf.api.spec.basePath,
            version: conf.api.spec.version || pkg.version ,
            tags: [
                {
                    name: "API",
                    description: " Notification API",
                    externalDocs: {
                        description: "Swagger",
                        url: "http://swagger.io"
                    }
                }
            ],
            securityDefinitions: {
                basic_auth: {
                    type: "http",
                    scheme: "basic"
                },
                jwt: {
                    type: "http",
                    scheme: "bearer",
                    bearerFormat: "JWT"
                } as any,
                api_key: {
                    type: "apiKey",
                    in: "query",
                    name: "api_key"
                },
                tsoa_auth: {
                    type: "oauth2",
                    flows: {
                        implicit: {
                            authorizationUrl: "http://swagger.io/api/oauth/dialog",
                            scopes: {
                                "write:pets": "modify things",
                                "read:pets": "read things"
                            }
                        }
                    }
                } as any
            },
            outputDirectory: `${swaggerOutputDirectory}`,
        };

        const routesConfig: ExtendedRoutesConfig = {
            ...commonConfig,
            basePath: conf.api.basePath,
            routesDir: "src/generated",
            authenticationModule: "src/api/authentication.ts",
        };

        await generateRoutes(routesConfig);
        await generateSpec(specConfig);

        const apiBasePath = conf.api.basePath;

        app.use(apiBasePath, cors());
        app.use(apiBasePath, bodyParser.json());
        app.use(apiBasePath, bodyParser.urlencoded({ extended: true }));
        // app.use(apiBasePath, authBasicOrJwt(conf.auth));

        const routes = require(path.join(__dirname, "/generated/routes"));
        routes.RegisterRoutes(app);

        app.use(apiBasePath, (err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
            if (req.is("json") !== false) {
                res.status((err as any).status || 500).json({ error: err.message || err });
            } else {
                next(err);
            }
        });

        try {
            const swaggerPath = path.join(__dirname, "../", swaggerOutputDirectory, "swagger.json");
            const swaggerDoc = require(swaggerPath);
            app.use(apiBasePath, swaggerUi.serve, swaggerUi.setup(swaggerDoc));
        } catch (err) {
            log.error("Unable to load swagger.json", err);
        }
    })();
}
