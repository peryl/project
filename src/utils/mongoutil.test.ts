import { mongoIdToStr } from "./mongoutil";
import { ObjectID } from "mongodb";

describe("Mongo util", () => {

    it("convert _id of type ObjectID to id of type string", () => {
        const testId1 = "5bd8594a3f5bde28f5d1021c";
        const testId2 = "5bd8594da9986b2929dc4b56";

        const testObject = {
            id: undefined as string,
            _id: new ObjectID(testId1),
            innerObject: {
                innerId: new ObjectID(testId2)
            }
        };
        mongoIdToStr(testObject);

        expect(testObject.id).toEqual(testId1);
        expect(testObject.innerObject.innerId).toEqual(testId2);
    });

});
