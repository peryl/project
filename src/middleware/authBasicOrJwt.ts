import * as log4js from "log4js";
import { basename, extname } from "path";
import { Request, Response, NextFunction } from "express";
import * as expressBasicAuth from "express-basic-auth";
import * as jwt from "express-jwt";

const log = log4js.getLogger(basename(__filename, extname(__filename)));

export interface AuthBasicOrJwtConf {
    basic: expressBasicAuth.BasicAuthMiddlewareOptions;
    jwt: {
        secretOrPublicKey: string;
        tokenSubjectClaim: string;
    };
}


export const authBasicOrJwt = (conf: AuthBasicOrJwtConf) => {

    let secretOrPublicKey: string;
    let jwtAuth: jwt.RequestHandler;
    const basicAuth = expressBasicAuth(conf.basic);

    return (req: Request, res: Response, next: NextFunction) => {

        if (secretOrPublicKey !== conf.jwt.secretOrPublicKey) {
            log.debug("secretOrPublicKey changed", secretOrPublicKey, conf.jwt.secretOrPublicKey);
            secretOrPublicKey = conf.jwt.secretOrPublicKey;
            jwtAuth = jwt({ secret: secretOrPublicKey });
        }
        const authorization = req.headers.authorization;
        const authType = "Bearer ";
        if (authorization && authorization.startsWith(authType)) {
            jwtAuth(req, res, next);
        } else {
            basicAuth(req, res, next);
        }
    };
};
