import { basename, extname } from "path";
import * as log4js from "log4js";
import { Router, Request, Response } from "express";
import { authBasic } from "../middleware/authbasic";
import { rbac } from "../middleware/rbac";
import { hsmls2html } from "peryl/dist/hsml-html";
import { HElements } from "peryl/dist/hsml";

const log = log4js.getLogger(basename(__filename, extname(__filename)));

// import * as bodyParser from "body-parser";

// parse application/json
// const jsonParser = bodyParser.json();
// app.use(jsonParser);

// parse application/x-www-form-urlencoded
// const urlencodedParser = bodyParser.urlencoded({ extended: false });
// app.use(urlencodedParser);

// const textParser = bodyParser.text();
// app.use(textParser);

// import * as cookieParser from "cookie-parser";
// app.use(cookieParser());

// app.use(authBasic);


interface PageState {
    title: string;
    lang: string;
}

function page(state: PageState): HElements {
    return [
        "<!DOCTYPE html>",
        ["html", { lang: state.lang }, [
            ["head", [
                ["meta", { charset: "utf-8" }],
                ["meta", { "http-equiv": "X-UA-Compatible", content: "IE=edge,chrome=1" }],
                ["meta", { name: "viewport", content: "width=device-width, initial-scale=1.0, maximum-scale=1.0" }],
                ["meta", { name: "author", content: "Peter Rybar, pr.rybar@gmail.com" }],
                ["title", state.title],
                ["link", { rel: "icon", href: "favicon.ico", type: "image/x-icon" }],
                ["link", { rel: "stylesheet", href: "https://www.w3schools.com/w3css/4/w3.css" }],
                // ["link", { rel:"stylesheet", href:"assets/css/styles.css" }]
                ["meta", { id: "theme-color", name: "theme-color", content: "#37474F" }],
                ["meta", { name: "apple-mobile-web-app-capable", content: "yes" }],
                ["link", { rel: "manifest", href: "manifest.json" }]
            ]],
            ["body", [
                ["div.w3-container", [
                    ["h1", "Project"],
                    ["h2", "REST API"],
                    ["p", [
                        "Protected data (user: rybar, password: peter): ",
                        ["br"],
                        ["a", { target: "_blank", href: "user" }, "user"],
                        ["br"],
                        ["a", { target: "_blank", href: "users" }, "users"],
                    ]],
                    ["h2", "REST TSOA API"],
                    ["p", [
                        ["a", { target: "_blank", href: "api" }, "api"]
                    ]],
                    ["h2", "SSE"],
                    ["p", [
                        ["a", { target: "_blank", href: "sse/test?auth=peter:rybar", }, "test"],
                        ["br"],
                        ["a", { target: "_blank", href: "sse/send?auth=peter:rybar&uid=peter" }, "send"],
                        ["br"],
                        ["a", { target: "_blank", href: "sse/subs?auth=peter:rybar" }, "subscribed"]
                    ]]
                ]]
            ]]
        ]]
    ];
}

const router: Router = Router();

router.get("/",
    (req: Request, res: Response) => {
        res.set("Content-Type", "text/html");

        const pageState = {
            title: "Project",
            lang: "en"
        };

        // res.send(hsmls2htmls(page(pageState), false).join(""));

        hsmls2html(page(pageState), html => res.write(html), true);
        res.end();
    });

router.get("/session",
    (req: Request, res: Response) => {
        if (!req.session.count) {
            req.session.count = 1;
        } else {
            req.session.count++;
        }
        log.info(req.session.id, req.session.count);

        res.set("Content-Type", "text/plain");
        res.send(`${req.session.id}: ${req.session.count}`);
    });

router.get("/rbac",
    authBasic,
    rbac(["admin"]),
    (req: Request, res: Response, next) => {
        log.debug("rbac", (req as any).user, (req as any).auth);
        res.send(`user ${JSON.stringify((req as any).user)} auth ${JSON.stringify((req as any).auth)}`);
    });

router.get("/rbac-authbasic",
    authBasic,
    rbac(["admin", "user"]),
    (req: Request, res: Response, next) => {
        log.debug("rbac-authbasic", (req as any).user, (req as any).auth);
        res.send(`user ${JSON.stringify((req as any).user)} auth ${JSON.stringify((req as any).auth)}`);
    });

router.get("/user",
    authBasic,
    async (req: Request, res: Response, next) => {
        log.debug("user get", req.params, req.query);
        if ((req as any).auth) {
            const user = await req.app.logic.user()
                .findByLogin((req as any).auth.user);
            if (user) {
                res.json({
                    user: {
                        login: user.login,
                        name: user.name,
                        roles: user.roles
                    }
                });
            } else {
                res.sendStatus(404); // Not Found
            }
        } else {
            res.sendStatus(404); // Not Found
        }
    });

router.get("/users",
    authBasic,
    async (req: Request, res: Response, next) => {
        log.debug("users get", req.params, req.query);
        const users = await req.app.logic.user().find();
        res.json({
            users: users.map(u => {
                return {
                    login: u.login,
                    name: u.name,
                    password: u.password,
                    roles: u.roles
                };
            })
        });
    });

export const rootRouter: Router = router;
