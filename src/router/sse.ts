import { basename, extname } from "path";
import * as log4js from "log4js";
import { Router, Request, Response, NextFunction } from "express";

const log = log4js.getLogger(basename(__filename, extname(__filename)));

const auth = {
    peter: "rybar"
};

interface Sub {
    res: Response;
    uid: string;
    sid: string;
    ts: Date;
}

let subs: Sub[] = [];

export function sseSend(data: any, uid?: string, sid?: string) {
    log.info("sseSend:", data, uid, sid);
    subs
        .filter(sub =>
            uid === undefined && sid === undefined ||
            (uid && uid === sub.uid && sid === undefined) ||
            (uid && uid === sub.uid && sid && sid === sub.sid)
        )
        .forEach((sub, i) => {
            const d = JSON.stringify(data);
            sub.res.write(`data: ${d}\n\n`);
            (sub.res as any).flush();
        });
}

const router: Router = Router();

router.get("/",
    authUser(auth),
    (req: Request, res: Response) => {
        res.set({
            "Cache-Control": "no-cache",
            "Content-Type": "text/event-stream",
            "Connection": "keep-alive"
            // "Access-Control-Allow-Origin": "*"
        });
        res.flushHeaders();
        // res.connection.setTimeout(0);

        // tell client retry every 10 seconds if connectivity is lost
        res.write(`retry: 10000\n\n`);
        (res as any).flush();

        const sub = {
            res,
            uid: (req as any).user,
            sid: req.query.sid,
            ts: new Date()
        };

        req.on("close", () => {
            const i = subs.indexOf(sub);
            if (i > -1) {
                subs.splice(i, 1);
            }
            log.info("sse close", i, { uid: sub.uid, sid: sub.sid, ts: sub.ts });
        });

        subs.push(sub);
        log.info("sse connect", req.query.id, subs.length);
    });

router.get("/subs",
    authUser(auth),
    (req: Request, res: Response) => {
        res.json(subs.map(s => ({ uid: s.uid, sid: s.sid, ts: s.ts })));
    });

router.get("/test",
    authUser(auth),
    (req: Request, res: Response) => {
        res.send(`<!DOCTYPE html>
        <html>
            <body>
                <script type="text/javascript">
                    // var es = new EventSource("sse?auth=\${Object.entries(auth)[0][0]}:\${Object.entries(auth)[0][1]}");
                    var es = new EventSource("./?sid=${new Date().getTime()}&auth=peter:rybar");
                    es.onmessage = function (e) {
                        console.log('message', e);
                        document.body.innerHTML += e.data + "<br>";
                    };
                    es.onopen = function (e) {
                        console.log('open', e);
                        document.body.innerHTML += "OPEN" + "<br>";
                    };
                    es.onerror = function (e) {
                        console.log('error', e);
                        document.body.innerHTML += "ERROR" + "<br>";
                    };
                </script>
            </body>
        </html>`);
        let count = 1;
        req.on("close", () => {
            log.info("sse close");
            count = 0;
        });
        function sendEvent() {
            const data = "event " + String(count) + " " + (count ** 2);
            log.info("send:", data);
            sseSend(data);
            count && setTimeout(sendEvent, (count ** 2) * 1e3);
            count++;
        }
        setTimeout(sendEvent, 1e3);
});

router.get("/send",
    authUser(auth),
    (req: Request, res: Response) => {
        // const data = "event " + new Date();
        const data = { x: 1971083, y: 1381491, d: new Date(), c: false };
        log.info("send:", data, req.query.uid, req.query.sid);
        sseSend(data, req.query.uid, req.query.sid);
        res.send(data);
    });

export const sseRouter: Router = router;

function authUser(auth: { [user: string]: string; }) {
    return (req: Request, res: Response, next: NextFunction) => {
        if (req.query.auth) {
            const [user, password] = (req.query.auth as string).split(":");
            if (user && auth[user] === password) {
                (req as any).user = user;
                return next();
            }
        }
        const err = new Error("Not authorized");
        (err as any).status = 400;
        next(err);
    };
}
