import * as log4js from "log4js";
import { basename, extname } from "path";
import { DbStore } from "../store/dbstore";
import { User } from "../model/user";

const log = log4js.getLogger(basename(__filename, extname(__filename)));

export interface Paging {
    offset: number;
    limit: number;
    total?: number;
}

export class UserLogic {

    private static _collection = "user";

    private _dbStore: DbStore;

    constructor(db: DbStore) {
        this._dbStore = db;
    }

    async removeAll(): Promise<void> {
        const db = await this._dbStore.conn();
        return new Promise<void>((resolve, reject) => {
            db.collection<User>(UserLogic._collection)
                .deleteMany({}, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    }

    async insert(user: User): Promise<string> {
        const db = await this._dbStore.conn();
        return new Promise<string>((resolve, reject) => {
            db.collection<User>(UserLogic._collection)
                .insertOne(user, (err, res) => {
                    if (err) {
                        log.error(err);
                        reject(err);
                    } else {
                        log.debug("User inserted:", user);
                        resolve(res.insertedId.toHexString());
                    }
                });
        });
    }

    async find(paging: Paging = { offset: 0, limit: 10 }): Promise<User[]> {
        const db = await this._dbStore.conn();
        return new Promise<User[]>((resolve, reject) => {
            db.collection<User>(UserLogic._collection)
                .find()
                .skip(paging.offset)
                .limit(paging.limit)
                .toArray((err, res) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                });
        });
    }

    async findById(id: string): Promise<User> {
        const db = await this._dbStore.conn();
        return new Promise<User>((resolve, reject) => {
            db.collection<User>(UserLogic._collection)
                .findOne({ id: id }, (err, res) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                });
        });
    }

    async findByLogin(login: string): Promise<User> {
        const db = await this._dbStore.conn();
        return new Promise<User>((resolve, reject) => {
            db.collection<User>(UserLogic._collection)
                .findOne({ login: login }, (err, res) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                });
        });
    }

}
